# skeletone

## a skeleton for the will

The will is the muscle of the soul, but muscle without skeleton lacks direction. Many people say they lack willpower, but what they usually lack is *structure*. What is the use of a contracting/relaxing tissue without the framework given by levers and pulleys?

This simple tool could help you with this by providing an audio file with any structure you design. Its name comes from skeleton + tone, since the idea is to produce tones (and voice messages) to provide a skeleton/schedule for a given task/session.

## installation

```
git clone https://codeberg.org/aleix_alva/skeletone
cd skeletone
chmod +x skeletone
```

The first line will download the files. The second will enter the directory and the third will give execution permissions to the (Bash) script.

## usage

Write your session on a text file (like the sample.txt). It must have three columns, separated by a semicolon. The first column contains the description of the exercises. The second and third column contain, in seconds, the duration of the exercise and the duration of the rest after it, respectively.

```
exercise 1;3;5
this, is the second exercise;3;5
and, now, the final one;3;1
```
In this case, the first exercise lasts 3 seconds, with 5 seconds of rest after that. The second exercise lasts 3 seconds and again offers a 5 s rest. And so on.

If you provide a 0 for the rest field, the program will directly jump to the next exercise.

Once the text file is ready (any extension will work), execute as

```
./skeletone filename.txt
```

It always generates an OGG audio file with the same name as your filename (in this case, filename.ogg).

## dependencies

You need to have these installed:

- sox
- espeak-ng
- vorbis-tools (oggenc)

## PATH or alias

If you want to access this program from any location, either add

```
export PATH=$PATH:/your/path/to/skeletone
```
to you `/home/user/.bashrc` file, or simply create an alias:

alias skeletone="~/my-path/skeletone/./skeletone"

Then, you can execute it from anywhere, as

```
skeletone filename.txt
```

